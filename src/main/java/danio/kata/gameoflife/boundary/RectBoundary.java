package danio.kata.gameoflife.boundary;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import danio.kata.gameoflife.model.Position;

public class RectBoundary implements Boundary {

    private final Position start;
    private final Position end;

    public RectBoundary(Position start, Position end) {
        this.start = checkNotNull(start);
        this.end = checkNotNull(end);
        checkArgument(end.getX() - start.getX() > 0);
        checkArgument(end.getY() - start.getY() > 0);
    }

    @Override
    public boolean contains(Position p) {
        return p.getX() >= start.getX()
                && p.getX() <= end.getX()
                && p.getY() >= start.getY()
                && p.getY() <= end.getY();
    }
}