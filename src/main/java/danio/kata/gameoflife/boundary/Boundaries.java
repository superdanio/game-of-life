package danio.kata.gameoflife.boundary;

import danio.kata.gameoflife.model.Position;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Boundaries {

    public static Boundary rect(Position start, Position end) {
        return new RectBoundary(start, end);
    }

}
