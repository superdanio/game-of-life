package danio.kata.gameoflife.boundary;

import danio.kata.gameoflife.model.Position;

@FunctionalInterface
public interface Boundary {

    boolean contains(Position position);

}
