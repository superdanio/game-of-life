package danio.kata.gameoflife.model;

import static java.util.Objects.requireNonNull;

import java.util.Set;

import lombok.Data;

@Data
public class Citizen {

    private final Position position;
    private final Set<Position> sphereOfInfluence;

    public Citizen(Position position) {
        this.position = requireNonNull(position);
        this.sphereOfInfluence = position.getRadius();
    }

    public static Citizen living(Position position) {
        return new Citizen(position);
    }

    public boolean isNextTo(Citizen other) {
        return position.isNextTo(other.position);
    }

}
