package danio.kata.gameoflife.model;

import java.util.Set;

import com.google.common.collect.ImmutableSet;

import lombok.Data;

@Data(staticConstructor = "at")
public class Position {

    private final int x;
    private final int y;

    public boolean isNextTo(Position other) {
        return !equals(other) && Math.abs(x - other.x) <= 1 && Math.abs(y - other.y) <= 1;
    }

    public Set<Position> getRadius() {
        return ImmutableSet.<Position>builder()
                .add(Position.at(x - 1, y - 1))
                .add(Position.at(x, y - 1))
                .add(Position.at(x + 1, y - 1))
                .add(Position.at(x - 1, y))
                .add(Position.at(x + 1, y))
                .add(Position.at(x - 1, y + 1))
                .add(Position.at(x, y + 1))
                .add(Position.at(x + 1, y + 1))
                .build();
    }

    @Override
    public String toString() {
        return String.format("(%s,%s)", x, y);
    }

}
