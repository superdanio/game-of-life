package danio.kata.gameoflife.rule;

import java.util.Set;

import danio.kata.gameoflife.model.Citizen;

@FunctionalInterface
public interface EvolutionRule {

    Set<Citizen> evolveFrom(Set<Citizen> generation);

}
