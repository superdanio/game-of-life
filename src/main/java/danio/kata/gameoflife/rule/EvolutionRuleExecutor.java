package danio.kata.gameoflife.rule;

import static java.util.Arrays.asList;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Collection;
import java.util.Set;

import com.google.common.base.Preconditions;

import danio.kata.gameoflife.model.Citizen;

public class EvolutionRuleExecutor implements EvolutionRule {

    private final Collection<EvolutionRule> rules;

    public EvolutionRuleExecutor(Collection<EvolutionRule> rules) {
        this.rules = Preconditions.checkNotNull(rules);
        checkArgument(!rules.isEmpty());
    }

    public static EvolutionRuleExecutor chainOf(EvolutionRule... rules) {
        return new EvolutionRuleExecutor(asList(rules));
    }

    @Override
    public Set<Citizen> evolveFrom(Set<Citizen> generation) {
        return rules.stream().reduce(generation, (res, rule) -> rule.evolveFrom(res), (a, b) -> a);
    }

}
