package danio.kata.gameoflife.rule;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import danio.kata.gameoflife.model.Citizen;
import danio.kata.gameoflife.model.Position;

public class ConwayEvolutionRule implements EvolutionRule {

    @Override
    public Set<Citizen> evolveFrom(Set<Citizen> generation) {
        Set<Citizen> survivors = generation.stream().filter(c -> isSurvivor(generation, c)).collect(toSet());

        Map<Position, Long> influencePositions = generation.stream()
                .map(Citizen::getSphereOfInfluence)
                .flatMap(Collection::stream)
                .collect(groupingBy(identity(), counting()));

        Set<Citizen> newborns = influencePositions.entrySet().stream()
                .filter(e -> e.getValue() == 3)
                .map(Entry::getKey)
                .map(Citizen::new)
                .collect(toSet());

        return ImmutableSet.<Citizen>builder().addAll(survivors).addAll(newborns).build();
    }

    private boolean isSurvivor(Set<Citizen> generation, Citizen citizen) {
        long neighbours = generation.stream().filter(citizen::isNextTo).collect(counting());
        return neighbours == 2 || neighbours == 3;
    }

}
