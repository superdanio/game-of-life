package danio.kata.gameoflife.rule;

import static java.util.stream.Collectors.toSet;

import java.util.Set;

import danio.kata.gameoflife.boundary.Boundary;
import danio.kata.gameoflife.model.Citizen;

public class BoundaryConstrainEvolutionRule implements EvolutionRule {

    private final Boundary boundary;

    public BoundaryConstrainEvolutionRule(Boundary boundary) {
        this.boundary = boundary;
    }

    @Override
    public Set<Citizen> evolveFrom(Set<Citizen> generation) {
        return generation.stream().filter(this::livesWithinBoundary).collect(toSet());
    }

    private boolean livesWithinBoundary(Citizen citizen) {
        return boundary.contains(citizen.getPosition());
    }

}
