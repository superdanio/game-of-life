package danio.kata.gameoflife.boundary;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import static danio.kata.gameoflife.boundary.Boundaries.rect;
import static danio.kata.gameoflife.model.Position.at;

import org.junit.Test;

public class RectBoundaryTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenStartAndEndHaveSameX() {
        rect(at(0, 0), at(0, 2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenStartAndEndHaveSameY() {
        rect(at(0, 0), at(2, 0));
    }

    @Test
    public void shouldReturnTrueWhenPositionIsInsideRectArea() {
        assertThat(rect(at(0, 0), at(2, 2)).contains(at(1, 1)), is(true));
    }

    @Test
    public void shouldReturnTrueWhenPositionIsInRectPerimeter() {
        assertThat(rect(at(0, 0), at(2, 2)).contains(at(1, 0)), is(true));
        assertThat(rect(at(0, 0), at(2, 2)).contains(at(2, 1)), is(true));
        assertThat(rect(at(0, 0), at(2, 2)).contains(at(1, 2)), is(true));
        assertThat(rect(at(0, 0), at(2, 2)).contains(at(0, 1)), is(true));
    }

    @Test
    public void shouldReturnFalseWhenPositionIsOutsideRectArea() {
        assertThat(rect(at(0, 0), at(3, 4)).contains(at(-1, 0)), is(false));
        assertThat(rect(at(0, 0), at(3, 4)).contains(at(5, 0)), is(false));
        assertThat(rect(at(0, 0), at(3, 4)).contains(at(0, -1)), is(false));
        assertThat(rect(at(0, 0), at(3, 4)).contains(at(0, 5)), is(false));
        assertThat(rect(at(0, 0), at(3, 4)).contains(at(5, 5)), is(false));
        assertThat(rect(at(0, 0), at(3, 4)).contains(at(-5, -5)), is(false));
        assertThat(rect(at(0, 0), at(3, 4)).contains(at(5, -5)), is(false));
        assertThat(rect(at(0, 0), at(3, 4)).contains(at(-5, 5)), is(false));
    }

}
