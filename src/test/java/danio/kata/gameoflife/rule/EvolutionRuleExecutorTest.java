package danio.kata.gameoflife.rule;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import static danio.kata.gameoflife.model.Position.at;
import static danio.kata.gameoflife.rule.EvolutionRuleExecutor.chainOf;

import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;

import danio.kata.gameoflife.model.Citizen;
import danio.kata.gameoflife.model.Position;
import danio.kata.gameoflife.rule.EvolutionRule;

public class EvolutionRuleExecutorTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenRulesAreEmpty() {
        chainOf();
    }

    @Test
    public void shouldApplyAllRulesInOrder() {
        EvolutionRule rule1 = expectingRule(citizens(at(0, 0)), citizens(at(1, 1)));
        EvolutionRule rule2 = expectingRule(citizens(at(1, 1)), citizens(at(2, 2)));
        EvolutionRule rule3 = expectingRule(citizens(at(2, 2)), citizens(at(3, 3)));

        assertThat(chainOf(rule1, rule2, rule3).evolveFrom(citizens(at(0, 0))), is(equalTo(citizens(at(3, 3)))));

        verify(rule1).evolveFrom(citizens(at(0, 0)));
        verify(rule2).evolveFrom(citizens(at(1, 1)));
        verify(rule3).evolveFrom(citizens(at(2, 2)));
        verifyNoMoreInteractions(rule1, rule2, rule3);
    }

    private EvolutionRule expectingRule(Set<Citizen> from, Set<Citizen> to) {
        EvolutionRule rule = Mockito.mock(EvolutionRule.class);
        when(rule.evolveFrom(from)).thenReturn(to);
        return rule;
    }

    private Set<Citizen> citizens(Position... positions) {
        return stream(positions).map(Citizen::living).collect(toSet());
    }

}
