package danio.kata.gameoflife.rule;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

import static danio.kata.gameoflife.model.Position.at;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import danio.kata.gameoflife.model.Citizen;
import danio.kata.gameoflife.rule.ConwayEvolutionRule;

public class ConwayEvolutionRuleTest {

    @Test
    public void shouldReturnNewGenerationApplyingConwayRules() {
        Set<Citizen> currentGeneration = new HashSet<>(asList(
                Citizen.living(at(1, 1)),
                Citizen.living(at(4, 1)),
                Citizen.living(at(4, 3)),
                Citizen.living(at(2, 4)),
                Citizen.living(at(3, 4)),
                Citizen.living(at(4, 4))));

        assertThat(new ConwayEvolutionRule().evolveFrom(currentGeneration), containsInAnyOrder(
                Citizen.living(at(4, 3)),
                Citizen.living(at(3, 4)),
                Citizen.living(at(4, 4)),
                Citizen.living(at(3, 5))));
    }

}
