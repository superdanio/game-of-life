package danio.kata.gameoflife.rule;

import static danio.kata.gameoflife.model.Position.at;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import danio.kata.gameoflife.boundary.Boundary;
import danio.kata.gameoflife.model.Citizen;

public class BoundaryConstrainEvolutionRuleTest {

    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private Boundary boundary;

    @Test
    public void shouldRemoveCitizensOutsideTheBoundary() {
        when(boundary.contains(at(0, 0))).thenReturn(true);
        when(boundary.contains(at(1, 1))).thenReturn(true);

        Set<Citizen> currentGeneration = new HashSet<>(asList(
                Citizen.living(at(-1, -1)),
                Citizen.living(at(-1, 1)),
                Citizen.living(at(0, 0)),
                Citizen.living(at(1, 1))));

        assertThat(new BoundaryConstrainEvolutionRule(boundary).evolveFrom(currentGeneration),
                containsInAnyOrder(Citizen.living(at(0, 0)), Citizen.living(at(1, 1))));

        verify(boundary).contains(at(-1, -1));
        verify(boundary).contains(at(-1, 1));
        verify(boundary).contains(at(0, 0));
        verify(boundary).contains(at(1, 1));
        verifyNoMoreInteractions(boundary);
    }

}
